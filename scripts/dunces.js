import secrets from "../secrets.js";

let FIRST_REWARD;
let SECOND_REWARD;

const dunceTemplate = (userId, displayName, order) => `
  <div class="dunces__container animate__animated animate__slow animate__bounceInLeft" data-order="${order}" data-userId="${userId}">
    <img class="dunces__person-image" src="../assets/comfy${
      order === 1 ? "1st" : "2nd"
    }.png" alt="${userId}" />
    <span class="dunces__person-name">${displayName}</span>
  </div>
`;

const getFirstReward = async () => {
  let newFirstReward;

  const channelRewards = await ComfyJS.GetChannelRewards(
    secrets.CLIENT_ID,
    false
  );
  // find if reward exists
  newFirstReward = channelRewards?.filter(({ title }) => title === "FIRST!");

  // create if doesn't exist
  if (newFirstReward.length === 0) {
    newFirstReward = await ComfyJS.CreateChannelReward(secrets.CLIENT_ID, {
      title: "FIRST!",
      prompt: "You are first to the stream!",
      cost: 1,
      is_enabled: true,
      background_color: "#136447",
      is_user_input_required: false,
      is_max_per_stream_enabled: false,
      max_per_stream: 0,
      is_max_per_user_per_stream_enabled: false,
      max_per_user_per_stream: 0,
      is_global_cooldown_enabled: true,
      global_cooldown_seconds: 5,
      should_redemptions_skip_request_queue: false,
    });

    return newFirstReward;
  }

  newFirstReward = newFirstReward[0];
  if (newFirstReward?.is_paused) {
    console.log("Unpausing FIRST! reward.");

    newFirstReward = updateRewardPause(newFirstReward, false);

    return newFirstReward;
  }

  return newFirstReward;
};

const getSecondReward = async () => {
  let newSecondReward;

  const channelRewards = await ComfyJS.GetChannelRewards(
    secrets.CLIENT_ID,
    false
  );
  // find if reward exists
  newSecondReward = channelRewards?.filter(({ title }) => title === "SECOND");

  // create if doesn't exist
  if (newSecondReward.length === 0) {
    newSecondReward = await ComfyJS.CreateChannelReward(secrets.CLIENT_ID, {
      title: "SECOND",
      prompt: "You came in second! So close!",
      cost: 2,
      is_enabled: true,
      background_color: "#2a975b",
      is_user_input_required: false,
      is_max_per_stream_enabled: false,
      max_per_stream: 0,
      is_max_per_user_per_stream_enabled: false,
      max_per_user_per_stream: 0,
      is_global_cooldown_enabled: true,
      global_cooldown_seconds: 5,
      should_redemptions_skip_request_queue: false,
    });

    newSecondReward = updateRewardPause(newSecondReward, true);

    return newSecondReward;
  }

  newSecondReward = newSecondReward[0];
  if (!newSecondReward?.is_paused) {
    console.log(
      "SECOND reward should not be enabled during initialization, pausing reward now."
    );
    newSecondReward = updateRewardPause(newSecondReward, true);

    return newSecondReward;
  }

  return newSecondReward;
};

const updateRewardPause = async (reward, paused = false) => {
  const updatedReward = await ComfyJS.UpdateChannelReward(
    secrets.CLIENT_ID,
    reward?.id,
    { is_paused: paused }
  );
  console.log(`Reward updated: ${reward?.title}`);
  return updatedReward;
};

const duncesContainer = document.getElementById("dunces");

// Initializing
ComfyJS.Init(secrets.TWITCH_NAME, secrets.ACCESS_KEY);
FIRST_REWARD = await getFirstReward();
SECOND_REWARD = await getSecondReward();
console.log("FIRST Reward found: ", FIRST_REWARD);
console.log("SECOND Reward found: ", SECOND_REWARD);

ComfyJS.onReward = (user, reward, cost, message, extra) => {
  const dunces = document.getElementById("dunces");
  console.log(extra);
  if (dunces.childNodes.length === 2) {
    return;
  }

  if (extra?.reward?.id === FIRST_REWARD?.id) {
    duncesContainer.insertAdjacentHTML(
      "beforeend",
      dunceTemplate(extra?.userId, user, 1)
    );
    updateRewardPause(FIRST_REWARD, true);
    updateRewardPause(SECOND_REWARD, false);
  }

  if (extra?.reward?.id === SECOND_REWARD?.id) {
    if (!dunces.innerHTML.includes(user)) {
      duncesContainer.insertAdjacentHTML(
        "beforeend",
        dunceTemplate(extra?.userId, user, 2)
      );

      updateRewardPause(SECOND_REWARD, true);
    }
  }
};
