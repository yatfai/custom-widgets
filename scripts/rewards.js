import secrets from "../secrets.js";

// duration in seconds
const SITTING_DURATION = 600;

let SITTING_REWARD;
const sittingUsers = {};

const sittingCornerTemplate = (userId, displayName) => `
  <div class="sitting-corner__person-container animate__animated animate__slow animate__bounceInDown" data-userId="${userId}">
    <img class="sitting-corner__person-image" src="../assets/peepoSit.webp" alt="${userId}" />
    <span class="sitting-corner__person-name">${displayName}</span>
  </div>
`;

const getSittingReward = async () => {
  let sittingReward;

  const channelRewards = await ComfyJS.GetChannelRewards(
    secrets.CLIENT_ID,
    false
  );

  sittingReward = channelRewards.filter(
    ({ title }) => title === "Sit with me!"
  );

  // create new sitting reward if not found
  if (sittingReward.length === 0) {
    console.log("Sitting Reward not found, creating one now...");
    sittingReward = await createSittingReward();

    return sittingReward;
  }

  // checking if reward is disabled
  sittingReward = sittingReward[0];
  if (sittingReward?.is_paused) {
    console.log(
      "Sitting Reward disabled during initialization! Re-enabling it now."
    );
    sittingReward = await ComfyJS.UpdateChannelReward(
      secrets.CLIENT_ID,
      sittingReward?.id,
      {
        is_paused: false,
      }
    );
  }

  return sittingReward;
};

const updateSittingReward = async (paused = false) => {
  SITTING_REWARD = await ComfyJS.UpdateChannelReward(
    secrets.CLIENT_ID,
    SITTING_REWARD?.id,
    {
      is_paused: paused,
    }
  );
  console.log("Sitting Reward updated: ", SITTING_REWARD);
};

const createSittingReward = async () => {
  const newSittingReward = await ComfyJS.CreateChannelReward(
    secrets.CLIENT_ID,
    {
      title: "Sit with me!",
      prompt:
        "WARNING: If you try to redeem this while you're already on the bench, the redeem do nothing and I will not refund you!",
      cost: 300,
      is_enabled: true,
      background_color: "#486D5D",
      is_user_input_required: false,
      is_max_per_stream_enabled: false,
      max_per_stream: 0,
      is_max_per_user_per_stream_enabled: false,
      max_per_user_per_stream: 0,
      is_global_cooldown_enabled: true,
      global_cooldown_seconds: 8,
      should_redemptions_skip_request_queue: false,
    }
  );

  return newSittingReward;
};

// const deleteSittingReward = async () => {
//   await ComfyJS.DeleteChannelReward(secrets.CLIENT_ID, SITTING_REWARD?.id);
// };

const createSittingPerson = (userId, displayName, maxSeats) => {
  // no duplicate sitters
  if (sittingUsers[userId]) {
    return;
  }

  const sittingCornerContainer = document.getElementById("sitting-corner");

  if (sittingCornerContainer.children.length >= maxSeats - 1) {
    console.log("Disabling reward: ", SITTING_REWARD?.title);
    updateSittingReward(true);
  }

  sittingCornerContainer.insertAdjacentHTML(
    "beforeend",
    sittingCornerTemplate(userId, displayName)
  );
  sittingUsers[userId] = displayName;

  setTimeout(() => {
    const removePerson = document.querySelector(`[data-userId="${userId}"]`);
    removePerson.classList.add("animate__animated", "animate__bounceOutUp");

    removePerson.addEventListener(
      "animationend",
      () => {
        console.log("Removing user: ", userId);
        removePerson.remove();
        delete sittingUsers[userId];
      },
      { once: true }
    );

    if (
      SITTING_REWARD?.is_paused &&
      sittingCornerContainer.children.length < maxSeats
    ) {
      console.log("Enabling reward: ", SITTING_REWARD?.title);
      updateSittingReward(false);
    }
  }, SITTING_DURATION * 1000);
};

// initializing
ComfyJS.Init(secrets.TWITCH_NAME, secrets.ACCESS_KEY);
SITTING_REWARD = await getSittingReward();
console.log("Sitting Reward found: ", SITTING_REWARD);

ComfyJS.onReward = (user, reward, cost, message, extra) => {
  if (extra?.reward?.id !== SITTING_REWARD?.id) {
    return;
  }

  const maxSeats = 5;

  createSittingPerson(extra?.userId, user, maxSeats);
};

ComfyJS.onCommand = async (user, command, message, flags, extra) => {
  if (flags?.broadcaster && command === "sitting") {
    switch (message) {
      case "disable":
        updateSittingReward(true);
        return;
      case "enable":
        SITTING_REWARD = await getSittingReward();
        return;
      default:
        return;
    }
  }
};
